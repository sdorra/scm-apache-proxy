#!/bin/bash
if [ ! -f /etc/apache2/ssl/server.key ]; then
  mkdir -p /etc/apache2/ssl
  KEY=/etc/apache2/ssl/server.key
  DOMAIN=$(hostname)
  export PASSPHRASE=$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 16)
  SUBJ="
C=DE
ST=Niedersachsen
O=SCM-Manager
localityName=Hildesheim
commonName=$DOMAIN
organizationalUnitName=SCM
emailAddress=admin@$DOMAIN
"
  openssl genrsa -des3 -out /etc/apache2/ssl/server.key -passout env:PASSPHRASE 2048
  openssl req -new -batch -subj "$(echo -n "$SUBJ" | tr "\n" "/")" -key $KEY -out /tmp/$DOMAIN.csr -passin env:PASSPHRASE
  cp $KEY $KEY.orig
  openssl rsa -in $KEY.orig -out $KEY -passin env:PASSPHRASE
  openssl x509 -req -days 365 -in /tmp/$DOMAIN.csr -signkey $KEY -out /etc/apache2/ssl/server.crt
fi

HOSTLINE=$(echo $(ip -f inet addr show eth0 | grep 'inet' | awk '{ print $2 }' | cut -d/ -f1) $(hostname) $(hostname -s))
echo $HOSTLINE >> /etc/hosts

# set scm backend
sed -i "s/%SCM%/$SCM_PORT_8080_TCP_ADDR/g" /etc/apache2/sites-available/default*

# finally, start docker
source /etc/apache2/envvars && exec /usr/sbin/apache2ctl -D FOREGROUND
