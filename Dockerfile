FROM debian:wheezy
MAINTAINER Sebastian Sdorra <s.sdorra@gmail.com>

RUN DEBIAN_FRONTEND=noninteractive apt-get update -y \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y apache2

ADD apache/default /etc/apache2/sites-available/default
ADD apache/default-ssl /etc/apache2/sites-available/default-ssl
ADD docker_entrypoint.sh /docker_entrypoint.sh

RUN a2enmod ssl proxy proxy_http rewrite \
    && a2ensite default-ssl \
    && chmod +x /docker_entrypoint.sh \
    && apt-get autoremove \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

VOLUME [ "/etc/apache2/ssl" ]
EXPOSE 80 443
CMD /docker_entrypoint.sh
